(defpackage #:daily-challenges/utils
  (:use #:cl
        #:split-sequence)
  (:nicknames #:utils)
  (:export #:get-number-from-user
           #:get-numbers-from-user))

(in-package #:utils)

(defun get-number-from-user ()
  (format t "Enter a number: ")
  (parse-integer (read-line)))

(defun get-numbers-from-user ()
  (format t "Enter a space-separated list of numbers: ")
  (mapcar #'parse-integer
          (split-sequence #\Space (read-line))))
