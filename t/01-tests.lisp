(defpackage #:daily-challenges-test/01
  (:use #:cl
        #:prove
        #:test-utils
        #:daily-challenges/01))

(in-package #:daily-challenges-test/01)

(plan 1)

(subtest "day 01"
  (subtest "detects elements that sum to k"
    (let ((inputs '(((10 15 3 7) . 17)
                    ((15 10 7 3) . 17)
                    ((1 2 3 4 5 6 7 8 9) . 17)
                    ((1 2 3 4 5 6 7 8 9) . 10)
                    ((-3 -2 -1 1 2 3) . 1)
                    ((-3 -2 -1 1 2 3) . -5)))
          (descriptions '("ANY-SUM-P returns T with digits at edges that sum to k"
                          "ANY-SUM-P returns T with inner digits that sum to k"
                          "ANY-SUM-P returns T with mixed digits that sum to k"
                          "ANY-SUM-P returns T with multiple pairs of digits that sum to k"
                          "ANY-SUM-P returns T when some values in the list are negative"
                          "ANY-SUM-P returns T when k is negative")))
      (loop :for input :in inputs
            :for description :in descriptions
            :do (ok (any-sum-p (first input) (rest input))
                    description))))
  (subtest "detects when no elements sum to k"
    (let ((inputs '(((1 2 3 4 5) . 10)))
          (descriptions '("ANY-SUM-P returns NIL when no digits sum to k")))
      (loop :for input :in inputs
            :for description :in descriptions
            :do (ok (not (any-sum-p (first input) (rest input)))
                    description))))
  (subtest "user interaction"
    (ok (with-test-input "1 2 3 4 5~%8~%"
          (main))
        "prompts user for input and then executes ANY-SUM-P")))

(finalize)
