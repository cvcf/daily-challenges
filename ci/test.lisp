#-(or daily-challenges prove sb-cover)
(ql:quickload '("daily-challenges" "prove" "sb-cover"))

(declaim (optimize sb-cover:store-coverage-data))

(asdf:test-system "daily-challenges" :force t)

(sb-cover:report "coverage/"
                 :if-matches (lambda (p)
                               (search "/src/" (directory-namestring p) :from-end t)))
