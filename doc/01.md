# Problem #1

## Problem Description

Given a list of numbers and a number `k`, return whether any two numbers from the list add up to `k`.

Bonus: Can you do this in one pass?

## Examples

Some sample nominal cases:

    (let ((l '(10 15 3 7))
          (k 17))
      (any-sum-p l k))
    ; => T

    (let ((l '(11 -15 -3 7))
          (k 8))
      (any-sum-p l k))
    ; => T

    (let ((l '(11 15 0 7))
          (k 15))
      (any-sum-p l k))
    ; => T

    (let ((l '(11 15 3 7))
          (k 17))
      (any-sum-p l k))
    ; => NIL
